using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorAsteroides : MonoBehaviour
{
    /// <summary>
    /// Prefab del asteroide para instanciar
    /// </summary>
    public GameObject prefabAsteroide;

    /// <summary>
    /// Coordenada pr�xima a la nave
    /// </summary>
    public float rangoInferior=3;

    /// <summary>
    /// Coordenada lejana a la nave
    /// </summary>
    public float rangoSuperior = 5;

    /// <summary>
    /// N�mero de asteroides a instanciar
    /// </summary>
    public int numAsteroides = 8;


    private void Start()
    {
        int nivelActual = PlayerPrefs.GetInt("nivel",0);
        int totalAsteroides = nivelActual + numAsteroides;

        //Instancia un n�mero de asteroides determinado
        for(int i = 0; i < totalAsteroides; i++)
        {
            GameObject instancia = Instantiate(prefabAsteroide, Vector3.zero, Quaternion.identity);
            
            //Desplaza el asteroide instanciado para alejarlo del centro
            instancia.transform.Translate(0, 0, Random.Range(rangoInferior, rangoSuperior));
            
            //Mueve el asteroide alrededor del centro
            instancia.transform.RotateAround(Vector3.zero,Vector3.up, 360/numAsteroides*i);

        }
    }

    void InstanciarAsteroides() { 
    }

    public void AsteroideDestruido(AsteroideTipo tipo, Vector3 posicion)
    {
        if (tipo != AsteroideTipo.PEQUE�O)
        {
            GameObject instancia1 = Instantiate(prefabAsteroide, posicion+(Vector3.left/2F), Quaternion.identity);
            GameObject instancia2 = Instantiate(prefabAsteroide, posicion+(Vector3.right / 2F), Quaternion.identity);

            PrepararInstancia(tipo, instancia1);
            PrepararInstancia(tipo, instancia2);

           // instancia1.GetComponent<Rigidbody>().AddExplosionForce(100, posicion, 0.25f);
            //instancia2.GetComponent<Rigidbody>().AddExplosionForce(100, posicion, 0.25f);
        }
    }

    void PrepararInstancia(AsteroideTipo tipo, GameObject instancia)
    {
        if (tipo == AsteroideTipo.GRANDE)
        {
            instancia.transform.localScale /= 2;
            instancia.GetComponent<Asteroide>().CambiarTipo(AsteroideTipo.MEDIANO);
            instancia.transform.GetChild(2).gameObject.SetActive(false);
            instancia.transform.GetChild(1).gameObject.SetActive(false);
            //instancia.transform.GetChild(0).gameObject.SetActive(false);
        }
        if (tipo == AsteroideTipo.MEDIANO)
        {
            instancia.transform.localScale /= 4;
            instancia.GetComponent<Asteroide>().CambiarTipo(AsteroideTipo.PEQUE�O);
            instancia.transform.GetChild(4).gameObject.SetActive(false);
            instancia.transform.GetChild(3).gameObject.SetActive(false);
            instancia.transform.GetChild(2).gameObject.SetActive(false);
            instancia.transform.GetChild(1).gameObject.SetActive(false);
        }
    }
}
