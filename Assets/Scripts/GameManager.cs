using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    /// <summary>
    /// Vidas del jugador
    /// </summary>
    int vidas = 3;

    /// <summary>
    /// Puntuaci�n conseguida en el nivel
    /// </summary>
    int puntuacion = 0;

    /// <summary>
    /// Variable de estado
    /// </summary>
    bool enJuego;

    /*
    public Image vida1;
    public Image vida2;
    public Image vida3;
    */

    public Image[] imagenesVidas;

    public Text puntuacionUI;

    private void Start()
    {
        Empezar();
    }

    void Empezar()
    {
        puntuacion = PlayerPrefs.GetInt("puntuacion", 0);
        puntuacionUI.text = puntuacion.ToString();
    }

    void GameOver()
    {

    }

    public void SumarPuntos(AsteroideTipo tipo)
    {
        switch (tipo)
        {
            case AsteroideTipo.GRANDE:
                puntuacion += 10;
                break;

            case AsteroideTipo.MEDIANO:
                puntuacion += 50;
                break;
            case AsteroideTipo.PEQUE�O:
                puntuacion += 100;
                break;
        }

        puntuacionUI.text = puntuacion.ToString();
    }

    public void ComprobarGameOver()
    {
        Asteroide[] asteroides = FindObjectsOfType<Asteroide>();
        if (asteroides.Length <= 1)
        {
            int nivel = PlayerPrefs.GetInt("nivel",0);
            nivel++;
            PlayerPrefs.SetInt("nivel",nivel);

            PlayerPrefs.SetInt("puntuacion", puntuacion);

            SceneManager.LoadScene(1);
        }
    }

    public void RestarVida()
    {
        vidas--;

        imagenesVidas[vidas].color = new Color(0, 0, 0, 0);

        if (vidas == 0)
        {
            PlayerPrefs.SetInt("puntuacion", puntuacion);
            SceneManager.LoadScene(2);
        }
    }

    public int ObtenerVidas()
    {
        return vidas;
    }
}
