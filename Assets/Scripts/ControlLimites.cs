using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Esta clase gestiona los l�mites de la pantalla
/// para que los objetos no se salgan
/// </summary>
public class ControlLimites : MonoBehaviour
{
    /// <summary>
    /// L�mite de la parte superior de la pantalla
    /// </summary>
    float limiteSuperior = 6.3f;

    /// <summary>
    /// L�mite derecho de la pantalla
    /// </summary>
    float limiteDer = 11f;

    /// <summary>
    /// Variable para referenciar el posible trail del objeto
    /// </summary>
    TrailRenderer trailObjeto;

    /// <summary>
    /// Variable para referenciar otros trails en los hijos
    /// </summary>
    TrailRenderer[] trailHijos;

    private void Start()
    {
        // Obtenemos los trails del objeto
        trailObjeto = GetComponent<TrailRenderer>();
        trailHijos = GetComponentsInChildren<TrailRenderer>();
    }

    void Update()
    {
        ComprobarLimites();   
    }

    /// <summary>
    /// Funci�n que recoloca el objeto si se sale de la pantalla.
    /// Lo recoloca en el borde opuesto
    /// </summary>
    void ComprobarLimites()
    {
        if (transform.position.x > limiteDer)
        {
            transform.position = new Vector3(-limiteDer, transform.position.y, transform.position.z);
            ResetearTrails();
        }
        if (transform.position.x < -limiteDer)
        {
            transform.position = new Vector3(limiteDer, transform.position.y, transform.position.z);
            ResetearTrails();
        }
        if (transform.position.z > limiteSuperior)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, -limiteSuperior);
            ResetearTrails();
        }
        if (transform.position.z < -limiteSuperior)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, limiteSuperior);
            ResetearTrails();
        }
    }

    /// <summary>
    /// Si el objeto tiene Trail Renderers, los resetea
    /// </summary>
    public void ResetearTrails()
    {
        if (trailObjeto != null)
        {
            trailObjeto.Clear();
        }

        if (trailHijos != null)
        {
            foreach(TrailRenderer tr in trailHijos)
            {
                tr.Clear();
            }
        }
    }
}
