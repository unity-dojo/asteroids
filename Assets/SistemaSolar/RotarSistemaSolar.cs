using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotarSistemaSolar : MonoBehaviour
{

    public float velocidadAngular = 1;

    private void Start()
    {
        transform.Rotate(0, Random.Range(0,360), 0);
    }

    void Update()
    {
        transform.Rotate(0, velocidadAngular, 0);
    }
}
