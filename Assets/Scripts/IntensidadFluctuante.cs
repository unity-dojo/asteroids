using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntensidadFluctuante : MonoBehaviour
{
    Light luz;

    private void Start()
    {
        luz = GetComponent<Light>();
    }

    void Update()
    {
        luz.intensity = Mathf.Sin(Time.time)*2+15;
        luz.range = Mathf.Sin(Time.time) * 2 + 28;
    }
}
